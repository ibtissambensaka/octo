import time
import streamlit as st
import requests
from PIL import Image

from eda.EDA import eda_fct
from training.Training import training_fct
from inference.Inference import inference_fct

st.title("Card Fraud Detection Dashboard")
st.sidebar.title("Data Themes")

sidebar_options = st.sidebar.selectbox(
    "Options",
    ("EDA", "Training", "Inference")
)

if sidebar_options == "EDA":
    eda_fct()
elif sidebar_options == "Training":
    training_fct()
else:
    inference_fct()
