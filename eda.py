import time
import streamlit as st
import requests
from PIL import Image
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split # used for splitting training and testing data
from sklearn.decomposition import PCA

class EDA:
    def eda_fct():
    st.header("Exploratory Data Analysis")
    st.info("In this section, you are invited to create insightful graphs "
            "about the card fraud dataset that you were provided.")
    #Data Frame
    st.subheader("Data Frame")
    st.table(df)
    st.subheader("Statistical Descriptions")
    st.table(df.describe())
    #Charts
    st.subheader("Charts")
    df.hist (figsize = (10,10))
    plt.show()
    st.pyplot()
    #Correlations
    st.subheader("Correlations")
    pd.plotting.scatter_matrix (df, figsize = (15,10))
    plt.show()
    st.pyplot()
    #Features using PCA
    st.subheader("Features: PCA")
    X = df.drop('Class',axis=1)
    y = df['Class']
    # splitting the dataset into training set and test set
    X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size=0.3, random_state=0)
    pca = PCA()
    # Transformation with its own data
    X_train = pca.fit_transform(X_train)
    X_test = pca.transform(X_test)
    pca_variance = pca.explained_variance_
    pca_variance
    plt.figure(figsize=(15, 6))
    plt.bar(range(85), pca_variance, alpha=0.5, align='center', label='individual variance')
    plt.legend()
    plt.ylabel('Variance ratio')
    plt.xlabel('Principal components')
    plt.show()
    st.pyplot()